import { getRandom, delay, AppError } from "./util";
import { fsm } from "./fluent-state-machine";

const games: Map<string, Game> = new Map();

export class Player {
  constructor(readonly name: string) {}
}

let lastQuestionId = 0;
export class Question {
  readonly questionId = lastQuestionId++;
  isOpen = true;
  started?: number;
  readonly answersByPlayer = new Map<Player, Answer>();
  readonly closeListeners: (() => void)[] = [];

  constructor(
    readonly numberOfOptions: number,
    readonly correctOptions: number[],
    readonly timeLimit: number,
  ) {
    if (
      numberOfOptions !== 2 &&
      numberOfOptions !== 3 &&
      numberOfOptions !== 4
    ) {
      throw new GameError("BAD_QUESTION", "There must be 2, 3 or 4 options");
    }

    if (!correctOptions.every(x => 1 <= x && x <= numberOfOptions)) {
      throw new GameError("BAD_QUESTION", "Incorrect 'correct' option");
    }
  }

  getVerdict(player: Player) {
    const answer = this.answersByPlayer.get(player);
    if (answer && this.started) {
      return {
        correct: this.correctOptions.includes(answer.option),
        answerSpeedInMilliseconds: answer.created - this.started,
        option: answer.option,
      };
    } else {
      return {
        correct: false,
        answerSpeedInMilliseconds: -1,
        option: -1,
      };
    }
  }

  onClose(callback: () => void) {
    this.closeListeners.push(callback);
  }

  /**
   * Attempt to close the question. If it was already closed, nothing happens.
   */
  close() {
    if (this.isOpen) {
      this.isOpen = false;

      for (const callback of this.closeListeners) {
        callback();
      }

      this.closeListeners.length = 0;
    }
  }
}

export class Answer {
  readonly created = Date.now();
  constructor(readonly option: number) {}
}

interface PlayerStats {
  playerName: string;
  answers: {
    correct: boolean;
    answerSpeedInMilliseconds: number;
    option: number;
  }[];
}

interface QuestionStats {
  option: number;
  selectedCount: number;
}

class GameStats {
  readonly stats: PlayerStats[];
  readonly latestQuestionStats: QuestionStats[];

  constructor(game: Game) {
    this.stats = this.getPlayerStats(game);
    this.latestQuestionStats = this.getLatestQuestionStats(game);
  }

  private getPlayerStats(game: Game): PlayerStats[] {
    return game.players.map(player => {
      return {
        playerName: player.name,
        answers: game.questions.map(question => question.getVerdict(player)),
      };
    });
  }

  private getLatestQuestionStats(game: Game): QuestionStats[] {
    const latestQuestion = game.questions[game.questions.length - 1];

    const result = new Array(latestQuestion.numberOfOptions)
      .fill(0)
      .map((_, i) => {
        return {
          option: i + 1,
          selectedCount: 0,
        };
      });

    for (const answer of Array.from(latestQuestion.answersByPlayer.values())) {
      const optionStats = result[answer.option - 1];
      if (optionStats) {
        optionStats.selectedCount++;
      }
    }

    return result;
  }
}

interface QuestionCallback {
  (question: Question): void;
}

interface Callbacks {
  question: QuestionCallback;
  questionEnded: QuestionCallback;
  gameComplete: () => void;
}

export class Game {
  private readonly machine = fsm("waiting")
    .event("playerConnect", {
      from: ["waiting"],
      fn: this.playerConnect.bind(this),
    })

    .event("nextQuestion", {
      from: ["waiting"],
      to: "questionActive",
      fn: this.nextQuestion.bind(this),
    })

    .event("playerAnswer", {
      from: ["questionActive"],
      fn: this.playerAnswer.bind(this),
    })

    .event("questionComplete", {
      from: ["questionActive"],
      to: "waiting",
      fn: this.questionComplete.bind(this),
    })

    .event("complete", {
      to: "completed",
      fn: this.complete.bind(this),
    })

    .build();

  readonly gameId = Game.createNewGameId();
  readonly password = getRandom() + getRandom() + getRandom();
  players: Player[] = [];
  readonly questions: Question[] = [];
  private readonly callbacksByPlayer = new Map<Player, Callbacks>();
  private fake = false;
  private readonly cleanupTimeout: ReturnType<typeof setTimeout>;

  constructor() {
    this.installStateMachineMethods();

    games.set(this.gameId, this);

    // Clean up after 4 hours.
    this.cleanupTimeout = setTimeout(
      () => this.machine.complete(),
      1000 * 60 * 60 * 4,
    );
  }

  private installStateMachineMethods() {
    for (const methodName in this.machine) {
      // Let TypeScript re-type the method name, for better error messages.
      const typedMethodName = methodName as keyof typeof Game.prototype.machine;

      if (typedMethodName === "current" || typedMethodName === "prev") {
        continue;
      }

      const fsmMethod = this.machine[typedMethodName];
      const gameMethod = this[typedMethodName];

      if (typeof fsmMethod !== "function" || typeof gameMethod !== "function") {
        throw new GameError(
          "INTERNAL_ERROR",
          `Expected both FSM#${methodName} and Game#${methodName} to be functions`,
        );
      }

      (this.machine as any)[methodName] = (this as any)[methodName] = (
        ...args: any
      ) => {
        try {
          return (fsmMethod as any)(...args);
        } catch (e) {
          // The FSM throws a string.
          if (typeof e === "string" && e.startsWith("Can't")) {
            throw new GameError("STATE_ERROR", e);
          } else {
            throw e;
          }
        }
      };
    }
  }

  static createFake() {
    const game = new Game();
    game.fake = true;
    game.fakeConnect();
    return game;
  }

  playerConnect(player: Player, callbacks: Callbacks) {
    if (!player.name) {
      throw new GameError("MISSING_PLAYER_NAME", "You must give a name");
    }

    if (this.players.some(x => x.name === player.name)) {
      throw new GameError(
        "PLAYER_NAME_NOT_UNIQUE",
        "There is already another player with this name",
      );
    }

    this.players.push(player);
    this.callbacksByPlayer.set(player, callbacks);
  }

  playerDisconnect(player: Player) {
    this.players = this.players.filter(x => x !== player);
    this.callbacksByPlayer.delete(player);
  }

  // TODO
  private async fakeConnect() {
    await delay(1000);
    this.players.push(new Player("Fake player"));
    await delay(1000);
    this.players.push(new Player("Another fake"));
    await delay(1000);
    this.players.push(new Player("Third fake"));
  }

  // TODO
  private fakeSendAnswers() {
    const latestQuestion = this.questions[this.questions.length - 1];
    for (const player of this.players) {
      setTimeout(() => {
        try {
          this.playerAnswer(
            player,
            latestQuestion.questionId,
            new Answer(
              1 + Math.floor(Math.random() * latestQuestion.numberOfOptions),
            ),
          );
        } catch (e) {
          // Do nothing.
        }
      }, Math.random() * 1.1 * latestQuestion.timeLimit * 1000);
    }
  }

  getStats(): GameStats {
    return new GameStats(this);
  }

  playerAnswer(player: Player, questionId: number, answer: Answer) {
    // Get the latest question.
    const question = this.questions[this.questions.length - 1];

    if (!question || question.questionId !== questionId) {
      throw new GameError(
        "CANNOT_ANSWER_THIS_QUESTION",
        "You tried to answer a question that isn't the latest one",
      );
    }

    if (!question.isOpen) {
      throw new GameError(
        "CANNOT_ANSWER_THIS_QUESTION",
        "You tried to answer a question that is currently closed",
      );
    }

    if (question.answersByPlayer.has(player)) {
      throw new GameError(
        "CANNOT_ANSWER_THIS_QUESTION",
        "You already answered this question",
      );
    }

    question.answersByPlayer.set(player, answer);

    if (question.answersByPlayer.size === this.players.length) {
      question.close();
    }
  }

  nextQuestion(question: Question): Promise<GameStats> {
    return new Promise(resolve => {
      this.questions.push(question);

      question.started = Date.now();

      if (this.fake) {
        this.fakeSendAnswers();
      } else {
        for (const player of this.players) {
          const cb = this.callbacksByPlayer.get(player);
          if (cb) {
            cb.question(question);
          }
        }
      }

      setTimeout(() => question.close(), question.timeLimit * 1000);

      // The question can also be closed if every player has answered it.
      question.onClose(() => {
        this.questionComplete(question);
        resolve(this.getStats());
      });
    });
  }

  private questionComplete(question: Question) {
    for (const player of this.players) {
      const cb = this.callbacksByPlayer.get(player);
      if (cb) {
        cb.questionEnded(question);
      }
    }
  }

  complete() {
    for (const player of this.players) {
      const cb = this.callbacksByPlayer.get(player);
      if (cb) {
        cb.gameComplete();
      }
    }

    games.delete(this.gameId);
    clearTimeout(this.cleanupTimeout);
  }

  static get(gameId: string, password: string) {
    const game = Game.getWithoutPassword(gameId);
    if (game.password !== password) {
      throw new GameError("WRONG_PASSWORD", "Wrong PIN or password");
    }

    return game;
  }

  static getWithoutPassword(gameId: string) {
    const game = games.get(gameId);
    if (!game) {
      throw new GameError(
        "GAME_NOT_FOUND",
        `Game with PIN '${gameId}' not found`,
      );
    }

    return game;
  }

  static createNewGameId(): string {
    // Use a short ID, but ensure that no existing game has the same ID.
    const rand = getRandom();
    for (let i = 3; i < rand.length; i++) {
      const id = rand.substr(0, i);
      if (!games.has(id)) {
        return id;
      }
    }

    // Failed to generate a random ID. Try again.
    return Game.createNewGameId();
  }

  static getDebug() {
    return Array.from(games.values()).map(game => ({
      gameId: game.gameId,
      fake: game.fake,
      players: game.players.map(p => p.name),
      questions: game.questions.map(q => ({
        questionId: q.questionId,
        isOpen: q.isOpen,
        started: q.started,
        numberOfOptions: q.numberOfOptions,
        timeLimit: q.timeLimit,
        answersByPlayer: game.players.map(
          p => q.answersByPlayer.get(p)?.option ?? null,
        ),
      })),
    }));
  }
}

type GameErrorCode = keyof typeof GameError.httpStatusCodes;

class GameError extends AppError {
  constructor(code: GameErrorCode, message: string) {
    super(GameError.httpStatusCodes[code], code, message);
  }

  static httpStatusCodes = {
    INTERNAL_ERROR: 500,
    STATE_ERROR: 400,
    GAME_NOT_FOUND: 404,
    BAD_QUESTION: 400,
    MISSING_PLAYER_NAME: 400,
    PLAYER_NAME_NOT_UNIQUE: 400,
    CANNOT_ANSWER_THIS_QUESTION: 400,
    WRONG_PASSWORD: 401,
  };
}
