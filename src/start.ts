import * as http from "http";
import * as WebSocket from "ws";
import { Game, Question, Player, Answer } from "./game";
import { getProcessArg, readFileAsync, AppError, NotFoundError } from "./util";
import { Logger, LogType } from "./logger";

const PORT = getProcessArg("number", "--port", 1234);
const logLevel = getProcessArg("string", "--log", "info") as LogType;
const logSize = getProcessArg("number", "--logSize", 100);
const wsPingInterval = getProcessArg("number", "--wsPingInterval", 50) * 1000;

let uniqueCounter = 1;
const log = new Logger(logLevel, logSize);

const server = http.createServer(router);
server
  .listen({
    host: "localhost",
    port: PORT,
  })
  .on("listening", () => {
    startWebSocketServer(server);

    log.info(`Listening on http://localhost:${PORT}...`);
  })
  .on("error", err => {
    log.error(err);
    process.exit(1);
  });

interface ParsedUrl {
  path: string[];
  queryParams: { [param: string]: string | undefined };
}

function parseUrl(
  url = "",
  pathRegexp = "",
  queryParams: string[] = [],
): ParsedUrl | undefined {
  const pathMatch = url.match(new RegExp(`^${pathRegexp}(?=\\?|$)`));

  if (!pathMatch) {
    return;
  }

  const queryParamsResult: {
    [key: string]: string | undefined;
  } = {};
  for (let queryParam of queryParams) {
    const isOptional = queryParam.endsWith("?");
    if (isOptional) {
      queryParam = queryParam.substr(0, queryParam.length - 1);
    }

    const queryStringMatch = url.match(
      new RegExp(`(\\?|&)${queryParam}(=([^&]*))?(&|$)`),
    );

    if (queryStringMatch) {
      queryParamsResult[queryParam] = decodeURIComponent(queryStringMatch[3]);
    } else if (isOptional) {
      queryParamsResult[queryParam] = undefined;
    } else {
      return;
    }
  }

  return {
    path: pathMatch.map((x, i) => (i === 0 ? x : decodeURIComponent(x))),
    queryParams: queryParamsResult,
  };
}

async function router(
  request: http.IncomingMessage,
  response: http.ServerResponse,
) {
  const logPrefix = "[router]";
  const url = request.url;
  let result;
  let parsed;

  function get(
    pathRegexp: string,
    queryParams: string[],
    callback: (parsed: ParsedUrl) => {} | Promise<{}>,
  ) {
    parsed = parseUrl(url, pathRegexp, queryParams);
    if (parsed) {
      return callback(parsed);
    } else {
      return false;
    }
  }

  async function getFile() {
    if (
      !url ||
      url.startsWith("/api/") ||
      (request.method !== "GET" && request.method !== "OPTIONS")
    ) {
      return;
    }

    let path = url.replace(/\?.*$/, "");
    if (
      path.includes("..") ||
      path.includes("//") ||
      /[^a-zA-Z0-9_./-]/.test(path)
    ) {
      return;
    }

    if (path.endsWith("/")) {
      path += "index.html";
    } else if (!/\.(html|css|js|mp3|jpg|png)$/.test(path)) {
      path += ".html";
    }

    path = "public/" + path.substr(1);

    try {
      return await readFileAsync(path);
    } catch (e) {
      new NotFoundError(`File '${path}' not found`);
    }
  }

  try {
    result =
      (await getFile()) ||
      get("/api/games/start", ["fake?"], parsed => {
        const fake = parsed.queryParams.fake === "true";
        const game = fake ? Game.createFake() : new Game();
        log.info(logPrefix, `Create game ${game.gameId} (fake=${fake})`);
        return {
          gameId: game.gameId,
          password: game.password,
          fake: fake,
        };
      }) ||
      get("/api/games/([0-9]+)/lobby", ["password"], parsed => {
        const game = Game.get(parsed.path[1], parsed.queryParams.password!);
        return {
          players: game.players,
        };
      }) ||
      get(
        "/api/games/([0-9]+)/question",
        ["password", "numberOfOptions", "correctOptions", "timeLimit"],
        async parsed => {
          const game = Game.get(parsed.path[1], parsed.queryParams.password!);
          const gameStats = await game.nextQuestion(
            new Question(
              +parsed.queryParams.numberOfOptions!,
              parsed.queryParams.correctOptions!.split(",").map(x => +x),
              +parsed.queryParams.timeLimit!,
            ),
          );
          log.info(logPrefix, `New question for game ${game.gameId}`);
          return gameStats;
        },
      ) ||
      get("/api/games/([0-9]+)/complete", ["password"], parsed => {
        const game = Game.get(parsed.path[1], parsed.queryParams.password!);
        game.complete();
        log.info(logPrefix, `Game ${game.gameId} complete`);
        return {
          message: "Game completed",
          stats: game.getStats(),
        };
      }) ||
      get("/api/debug", ["processId?", "games?", "logs?"], parsed => {
        const processId = parsed.queryParams.processId === "true" || undefined;
        const games = parsed.queryParams.games === "true" || undefined;
        const logs = parsed.queryParams.logs as LogType | undefined;

        if (processId || games || logs) {
          return {
            processId: processId && process.pid,
            games: games && Game.getDebug(),
            logs: logs && log.getLogs(logs),
          };
        } else {
          const logTypes = Logger.LOG_TYPES.join(", ");

          return {
            help: {
              processId: "Use query param processId=true",
              games: "Use query param games=true",
              logs: `Use query param logs=<logtype> (${logTypes})`,
            },
          };
        }
      }) ||
      (() => {
        throw new NotFoundError(`Unrecognized URI ${request.url}`);
      })();

    result = await result;
    log.debug(
      logPrefix,
      200,
      request.method,
      url &&
        url.replace(/([?&](password|correctOptions)=)[^&]+/g, "$1<redacted>"),
    );
  } catch (e) {
    if (e instanceof AppError) {
      if (url && url.startsWith("/api/")) {
        result = {
          error: true,
          message: e.message,
        };
      } else {
        result = `
          <!DOCTYPE html>
          <meta charset="utf-8">
          <title>404 Not Found</title>
          <style>html { font-family: sans-serif; }</style>
          <h1>404 Not Found</h1>
          <p>${e.message.replace(
            /[<>&"']/g,
            x => `&#${x.charCodeAt(0)};`,
          )}</p>`;
      }
      response.statusCode = e.statusCode;
      log.error(logPrefix, e.statusCode, request.method, url, e.message);
    } else {
      result = {
        error: true,
        message: "An internal error occurred",
        details: e + "",
      };
      response.statusCode = 500;
      response.statusMessage = "Internal Server Error";
      log.error(logPrefix, request.method, url, e);
    }
  }

  if (typeof result === "string" || result instanceof Buffer) {
    const extMatch = url && url.match(/^.*\.(js|css|png)$/);

    const ext: keyof typeof contentTypesByExt = extMatch
      ? (extMatch[1] as "js" | "css" | "png")
      : "html";
    const contentTypesByExt = {
      html: "text/html; charset=utf-8",
      js: "text/javascript",
      css: "text/css",
      png: "image/png",
    };
    const contentType = contentTypesByExt[ext];

    response.setHeader("Content-Type", contentType);
    response.write(result);
  } else {
    response.setHeader("Content-Type", "application/json");

    const stringified = JSON.stringify(result);
    if (stringified !== undefined) {
      response.write(stringified);
    }
  }

  response.end();
}

function startWebSocketServer(server: http.Server) {
  const webSocketServer = new WebSocket.Server({
    server: server,
    path: "/api/websocket",
  });

  webSocketServer.on("connection", socket => {
    const connectionId = uniqueCounter++;
    let connectionGame: { game: Game; player: Player } | undefined;
    const logPrefix = `[ws ${connectionId}]`;
    log.info(logPrefix, "New connection");

    const pingTimeout = setInterval(() => {
      socket.ping();
      log.debug(logPrefix, "Send ping");
    }, wsPingInterval);

    function disconnect() {
      if (connectionGame) {
        log.debug(logPrefix, "Disconnect");
        connectionGame.game.playerDisconnect(connectionGame.player);
        connectionGame = undefined;
      }
    }

    socket.on("close", () => {
      log.info(logPrefix, "Close socket");
      disconnect();
      clearInterval(pingTimeout);
    });

    socket.on("error", error => log.error(logPrefix, error));

    socket.on("ping", () => log.debug(logPrefix, "Got ping"));
    socket.on("pong", () => log.debug(logPrefix, "Got pong"));

    socket.on("message", data => {
      if (typeof data !== "string" || data === "") {
        return;
      }

      try {
        const message = JSON.parse(data);
        log.debug(logPrefix, `Message '${message.type}'`);
        switch (message.type) {
          case "disconnect":
            disconnect();
            break;

          case "connect": {
            disconnect();

            log.info(
              logPrefix,
              `Get game ${message.gameId} for player '${message.name}'`,
            );
            const game = Game.getWithoutPassword(message.gameId);
            const player = new Player(message.name);
            game.playerConnect(player, {
              question: question =>
                send({
                  type: "question",
                  numberOfOptions: question.numberOfOptions,
                  questionId: question.questionId,
                  timeLimit: question.timeLimit,
                }),
              questionEnded: question =>
                send({
                  type: "questionEnded",
                  questionId: question.questionId,
                  verdict: question.getVerdict(player),
                }),
              gameComplete: () =>
                send({
                  type: "gameComplete",
                }),
            });
            connectionGame = { game, player };
            break;
          }

          case "answer":
            if (connectionGame) {
              connectionGame.game.playerAnswer(
                connectionGame.player,
                message.questionId,
                new Answer(message.option),
              );
            }
            break;
        }
      } catch (e) {
        if (e instanceof AppError) {
          send({
            type: "error",
            code: e.code,
            message: e.message,
          });
        } else {
          send({
            type: "error",
            code: "UNKNOWN_ERROR",
            message: e + "",
          });
        }
      }
    });

    function send(message: { type: string; [prop: string]: unknown }) {
      if (message.type === "error") {
        log.error(logPrefix, message.code as string, message.message as string);
      } else {
        log.debug(logPrefix, `Send ${message.type}`);
      }

      socket.send(JSON.stringify(message));
    }
  });
}
