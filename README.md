# QuickQ

[QuickQ](https://danwolff.se/quickq/) is a quiz app. Its components are meant to be somewhat independent, allowing for different clients, presenter UI and servers to mix.
